<?php
/**
 * The api calls integrations functionality of the plugin.
 *
 * @link       https://cargus.ro/
 * @since      1.0.0
 *
 * @package    Cargus
 * @subpackage Cargus/admin
 */

if ( session_status() === PHP_SESSION_NONE ) {
	session_start(); // Start the session if it's not already started.
}

if ( ! class_exists( 'Cargus_Api' ) ) {
	/**
	 * The api calls integrations functionality of the plugin.
	 *
	 * Defines the plugin api calls method.
	 *
	 * @package    Cargus
	 * @subpackage Cargus/admin
	 * @author     Cargus <contact@cargus.ro>
	 */
	#[AllowDynamicProperties]
	class Cargus_Api {

		/**
		 * The api key of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $key    The api key of this plugin.
		 */
		private $key;

		/**
		 * The api url of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $url    The api url of this plugin.
		 */
		private $url;

		/**
		 * The export api url of this plugin.
		 *
		 * @since    1.5.0
		 * @access   private
		 * @var      string    $url    The api url of this plugin.
		 */
		private $export_url;

		/**
		 * The api username of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $username    The api username of this plugin.
		 */
		private $username;

		/**
		 * The api password of this plugin.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $password    The api password of this plugin.
		 */
		private $password;

		/**
		 * The api login token.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      string    $password    The api password of this plugin.
		 */
		public $token;

		/**
		 * The api login export token.
		 *
		 * @since    2.0
		 * @access   private
		 * @var      string    $password    The api password of this plugin.
		 */
		public $export_token;

		/**
		 * The single instance of the class.
		 *
		 * @since    1.0.0
		 * @access   private
		 * @var      Cargus_Api    $instance    The single instance of the class.
		 */
		private static $instance = null;

		/**
		 * Set the api key and url of the plugin.
		 *
		 * @since 1.0.0
		 * @param string $url       The api url of this plugin.
		 * @param string $key       The api key of this plugin.
		 * @param string $export    Check if the api login is for the export api.
		 */
		private function __construct( $url = '', $key = '', $export = false ) {

			require_once 'class-cargus-debug.php';

			$woocommerce_cargus_settings        = get_option( 'woocommerce_cargus_settings' );
			$woocommerce_cargus_export_settings = get_option( 'woocommerce_cargus_export_settings' );

			$this->url        = ( '' !== $url ) ? $url : ( $woocommerce_cargus_settings['webservice'] ?? '' );
			$this->key        = ( '' !== $key ) ? $key : ( $woocommerce_cargus_settings['apikey'] ?? '' );
			$this->export_url = $woocommerce_cargus_export_settings['webservice'] ?? '';
		}

		/**
		 * Get the single instance of the class.
		 *
		 * @since 1.0.0
		 * @param string $url       The api url of this plugin.
		 * @param string $key       The api key of this plugin.
		 * @param bool   $export    Whether to use the export API settings.
		 * @return Cargus_Api       The single instance of the Cargus_Api class.
		 */
		public static function get_instance( $url = '', $key = '' ) {
			if ( null === self::$instance ) {
				if ( isset( $_SESSION['cargus_api_instance'] ) ) {
					self::$instance = unserialize( $_SESSION['cargus_api_instance'] );
				} else {
					self::$instance                  = new self( $url, $key );
					$_SESSION['cargus_api_instance'] = serialize( self::$instance );
				}
			}

			return self::$instance;
		}

		/**
		 * Get a property from the cargus_api_instance session.
		 *
		 * @param string $property The property to retrieve.
		 * @return mixed The property value or null if not set.
		 */
		private function get_instance_property( string $property ) {
			if ( isset( $_SESSION['cargus_api_instance'] ) ) {
				$instance = unserialize( $_SESSION['cargus_api_instance'] );
				return $instance->$property ?? null;
			}
			return null;
		}

		/**
		 * Get the api key.
		 */
		public function get_api_key() {
			return $this->get_instance_property( 'key' );
		}

		/**
		 * Get the api url.
		 */
		public function get_url() {
			return $this->get_instance_property( 'url' );
		}

		/**
		 * Get the api export url.
		 */
		public function get_export_url() {
			return $this->get_instance_property( 'export_url' );
		}

		/**
		 * Set the export url.
		 *
		 * @since 1.5.0
		 * @param string $new_export_url The api export url of this plugin.
		 */
		public function set_export_url( $new_export_url ) {
			if ( ! empty( $new_export_url ) && filter_var( $new_export_url, FILTER_VALIDATE_URL ) ) {
				$this->export_url = $new_export_url;
				// Save the export url in the class instance.
				if ( session_status() === PHP_SESSION_ACTIVE && isset( $_SESSION['cargus_api_instance'] ) ) {
					$instance                        = unserialize( $_SESSION['cargus_api_instance'] );
					$instance->export_url            = $this->export_url;
					$_SESSION['cargus_api_instance'] = serialize( $instance );
				}
			} else {
				throw new InvalidArgumentException( 'Invalid URL provided.' );
			}
		}


		/**
		 * Call a specific api method.
		 *
		 * @since  1.0.0
		 * @param  string $function       The name of the api method.
		 * @param  string $method         The method of the api method.
		 * @param  string $parameters     The paramethers that are being sent.
		 * @param  bool   $token          The login token.
		 * @throws Exception Exception message.
		 */
		public function call_method( $function, $method, $parameters = '', $token = null ) {
			$url = $this->url . '/' . $function;
			$key = $this->key;

			$debugger = CargusDebug::get_instance();

			$debugger->debug( 'Calling Cargus API method: ' . $function );
			$debugger->debug( 'API URL: ' . $url );
			$debugger->debug( 'API Method: ' . $method );

			$args = array();
			if ( 'LoginUser' === $function ) {
				$args = array(
					'headers' => array(
						'Ocp-Apim-Subscription-Key' => $key,
						'Ocp-Apim-Trace'            => true,
						'Content-Type'              => 'application/json',
						'ContentLength'             => strlen( wp_json_encode( $parameters ) ),
					),
				);
			} else {
				$args = array(
					'headers' => array(
						'Ocp-Apim-Subscription-Key' => $key,
						'Ocp-Apim-Trace'            => true,
						'Authorization'             => 'Bearer ' . $token,
						'Content-Type'              => 'application/json',
					),
				);

				if ( 'Awbs' === $function && 'POST' === $method ) {
					$args['headers']['Path'] = 'WP' . substr( $this->get_cargus_get_woocommerce_version(), 0, 3 );
				}
			}

			switch ( $method ) {
				case 'POST':
					if ( ! empty( $parameters ) ) {
						$args['body'] = wp_json_encode( $parameters );
					}

					$response = wp_remote_post( $url, $args );
					break;

				case 'DELETE':
					if ( ! empty( $parameters ) ) {
						$args['body'] = wp_json_encode( $parameters );
					}
					$args['method'] = 'DELETE';
					$response       = wp_remote_request( $url, $args );
					break;

				case 'PUT':
					if ( ! empty( $parameters ) ) {
						$args['body'] = wp_json_encode( $parameters );
					}
					$args['method'] = 'PUT';
					$response       = wp_remote_request( $url, $args );
					break;

				case 'GET':
					$query_url = $url;
					if ( ! empty( $parameters ) ) {
						$query_url = $url . '?' . http_build_query( $parameters );
					}
					$response = wp_remote_get( $query_url, $args );
					break;

				default:
					throw new Exception( 'Unknown method used' );
			}

			if ( ! is_wp_error( $response ) ) {
				$code = wp_remote_retrieve_response_code( $response );
				$body = json_decode( $response['body'] );

				$debugger->debug( 'API Response Code: ' . $code );
				$debugger->debug( '---------------------------------' );
				if ( '200' === $code ) {
					if ( is_array( $body ) && isset( $body['message'] ) ) {
						return $body['message'];
					} else {
						return $body;
					}
				} elseif ( '500' === $code ) {
					return array(
						'statusCode' => 500,
						'message'    => $body,
					);
				} else {
					return $body;
				}
			}
		}

		/**
		 * Call a specific api method for international.
		 *
		 * @since  1.0.0
		 * @param  string $function       The name of the api method.
		 * @param  string $method         The method of the api method.
		 * @param  string $parameters     The paramethers that are being sent.
		 * @param  bool   $token          The login token.
		 * @throws Exception Exception message.
		 */
		public function call_method_intl( $function, $method, $parameters = '', $token = null ) {

			$woocommerce_cargus_export_settings = get_option( 'woocommerce_cargus_export_settings' );
			$debugger                           = CargusDebug::get_instance();

			if ( isset( $woocommerce_cargus_export_settings['webservice'] ) && isset( $woocommerce_cargus_export_settings['apikey'] ) ) {
				$url = $woocommerce_cargus_export_settings['webservice'];
				$key = $woocommerce_cargus_export_settings['apikey'];
				$url = $url . '/' . $function;
			} else {
				$debugger->error( 'Missing webservice or apikey in WooCommerce Cargus export settings' );
				return;
			}

			$debugger = CargusDebug::get_instance();

			$debugger->debug( 'International API URL: ' . $url );
			$debugger->debug( 'Calling Cargus API url: ' . $url );
			$debugger->debug( 'Calling Cargus API method: ' . $function );
			$debugger->debug( 'API URL: ' . $url );
			$debugger->debug( 'API Method: ' . $method );

			$args = array();

			if ( 'LoginUser' === $function ) {
				$args = array(
					'headers' => array(
						'Ocp-Apim-Subscription-Key' => $key,
						'Ocp-Apim-Trace'            => true,
						'Content-Type'              => 'application/json',
						'ContentLength'             => strlen( wp_json_encode( $parameters ) ),
					),
				);
			} else {
				$args = array(
					'headers' => array(
						'Ocp-Apim-Subscription-Key' => $key,
						'Ocp-Apim-Trace'            => true,
						'Authorization'             => 'Bearer ' . $token,
						'Content-Type'              => 'application/json',
					),
				);

				if ( 'Awbs' === $function && 'POST' === $method ) {
					$args['headers']['Path'] = 'WP' . substr( $this->get_cargus_get_woocommerce_version(), 0, 3 );
				}
			}

			switch ( $method ) {
				case 'POST':
					if ( ! empty( $parameters ) ) {
						$args['body'] = wp_json_encode( $parameters );
					}

					$response = wp_remote_post( $url, $args );
					break;

				case 'DELETE':
					if ( ! empty( $parameters ) ) {
						$args['body'] = wp_json_encode( $parameters );
					}
					$args['method'] = 'DELETE';
					$response       = wp_remote_request( $url, $args );
					break;

				case 'PUT':
					if ( ! empty( $parameters ) ) {
						$args['body'] = wp_json_encode( $parameters );
					}
					$args['method'] = 'PUT';
					$response       = wp_remote_request( $url, $args );
					break;

				case 'GET':
					$query_url = $url;
					if ( ! empty( $parameters ) ) {
						$query_url = $url . '?' . http_build_query( $parameters );
					}
					$response = wp_remote_get( $query_url, $args );
					break;

				default:
					throw new Exception( 'Unknown method used' );
			}

			if ( ! is_wp_error( $response ) ) {
				$code = wp_remote_retrieve_response_code( $response );
				$body = json_decode( $response['body'] );
				$debugger->debug( 'API Response Code: ' . $code );
				$debugger->debug( '---------------------------------' );
				if ( 200 === $code ) {
					if ( is_array( $body ) && isset( $body['message'] ) ) {
						return $body['message'];
					} else {
						return $body;
					}
				} elseif ( 500 === $code ) {
					return array(
						'statusCode' => 500,
						'message'    => $body,
					);
				} else {
					return $body;
				}
			}
		}

		/**
		 * Get the installed woocommerce version.
		 *
		 * @since    1.0.0
		 */
		private function get_cargus_get_woocommerce_version() {
			// If get_plugins() isn't available, require it.
			if ( ! function_exists( 'get_plugins' ) ) {
				require_once ABSPATH . 'wp-admin/includes/plugin.php';
			}

			// Create the plugins folder and file variables.
			$plugin_folder = get_plugins( '/woocommerce' );
			$plugin_file   = 'woocommerce.php';

			// If the plugin version number is set, return it.
			if ( isset( $plugin_folder[ $plugin_file ]['Version'] ) ) {
				return $plugin_folder[ $plugin_file ]['Version'];
			} else {
				// Otherwise return null.
				return null;
			}
		}

		/**
		 * Call cargus Login endpoint.
		 *
		 * @param array $fields   The API login token.
		 * @param bool  $is_export Whether to generate an export token.
		 *
		 * @return string The generated token.
		 * @throws Exception
		 */
		public function generate_token( array $fields = array(), bool $is_export = false ) {
			$debugger = CargusDebug::get_instance();

			// Determine the appropriate token option key and method.
			$token_option_key = $is_export ? 'cargus_export_login_token' : 'cargus_login_token';
			$token            = $is_export ? $this->call_method_intl( 'LoginUser', 'POST', $fields ) : $this->call_method( 'LoginUser', 'POST', $fields );

			if ( 'error' !== $token && ! is_object( $token ) && ! is_array( $token ) ) {
				if ( $is_export ) {
					$this->export_token = $token;
				} else {
					$this->token = $token;
				}

				// Save the token in the wp options.
				update_option( $token_option_key, $token, false );

				// Save the token in the class instance.
				if ( session_status() === PHP_SESSION_ACTIVE && isset( $_SESSION['cargus_api_instance'] ) ) {
					$instance = unserialize( $_SESSION['cargus_api_instance'] );
					if ( $is_export ) {
						$instance->export_token = $token;
					} else {
						$instance->token = $token;
					}
					$_SESSION['cargus_api_instance'] = serialize( $instance );
				}

				$debugger->debug( 'New ' . ( $is_export ? 'export ' : '' ) . 'token generated and saved to option: ' . $token_option_key );
			} else {
				$debugger->error( 'Error generating new ' . ( $is_export ? 'export ' : '' ) . 'token: ' . wp_json_encode( $token ) );
			}

			return $is_export ? $this->export_token : $this->token;
		}

		/**
		 * Helper function to fetch and cache data from the API.
		 *
		 * @param string $endpoint The API endpoint to call.
		 * @param string $transient_key The key for caching the data.
		 * @return mixed The JSON response from the API.
		 */
		private function fetch_data( $endpoint, $transient_key ) {
			$debugger        = CargusDebug::get_instance();
			$json            = get_transient( $transient_key );
			$now             = time();
			$next_cron       = wp_next_scheduled( 'cargus_refresh_login_token_action' );
			$expiration_time = $next_cron ? max( $next_cron - $now, 60 ) + 60 : strtotime( 'tomorrow midnight' ) - $now;

			// Check if data needs to be fetched from the API.
			if ( empty( $json ) || 'Failed to authenticate!' === $json || $now > ( get_option( 'last_cargus_refresh_login_token', 0 ) + $expiration_time ) ) {
				$debugger->debug( "Fetching new data for $endpoint." );
				$json = $this->call_method( $endpoint, 'GET', array(), get_option( 'cargus_login_token' ) );

				if ( ! empty( $json ) ) {
					set_transient( $transient_key, $json, $expiration_time );
					update_option( 'last_cargus_refresh_login_token', $now );
					$debugger->debug( "Data fetched and cached for $endpoint." );
				} else {
					$debugger->debug( "Fetched data is empty for $endpoint." );
				}
			} else {
				$debugger->debug( "Cached data is still valid for $endpoint." );
			}

			$debugger->debug( "$endpoint JSON: " . wp_json_encode( $json ) );
			return $json;
		}

		/**
		 * Fetch pickup locations from the API.
		 *
		 * @return mixed The JSON response containing pickup locations.
		 */
		public function get_pickup_locations() {
			return $this->fetch_data( 'PickupLocations', 'cached_pickup_locations' );
		}

		/**
		 * Fetch price tables from the API.
		 *
		 * @return mixed The JSON response containing price tables.
		 */
		public function get_price_tables() {
			return $this->fetch_data( 'PriceTables', 'cached_price_tables' );
		}

		/**
		 * Call cargus shipping calculation api endpoint.
		 *
		 * @param array $fields The shipping calculation fields.
		 */
		public function get_shipping_calulation( $fields ) {
			$json = array();
			$json = $this->call_method( 'ShippingCalculation', 'POST', $fields, get_option( 'cargus_login_token' ) );
			return $json;
		}

		/**
		 * Call cargus shipping calculation api endpoint for export.
		 *
		 * @param array $fields The shipping calculation fields.
		 *
		 * @throws Exception
		 */
		public function get_export_shipping_calculation( $fields ) {
			return $this->call_method( 'pricing/calculate', 'POST', $fields, get_option( 'cargus_login_token' ) );
		}


		/**
		 * Call cargus pudo points api endpoint.
		 */
		public function get_pudo_points() {
			$json = array();
			$json = $this->call_method( 'PudoPoints', 'GET', array(), get_option( 'cargus_login_token' ) );

			return $json;
		}

		/**
		 * Call cargus Streets api endpoint.
		 *
		 * @param int $locality_id The locality id.
		 */
		public function get_streets( $locality_id ) {
			// get streets.
			if ( is_null( $locality_id ) ) {
				return array();
			}

			$json = array();
			// get api json.
			// Streets?locality_id=97243.
			$json = $this->call_method( 'Streets?localityId=' . $locality_id, 'GET', array(), get_option( 'cargus_login_token' ) );

			if ( ! $json ) {
				// get old cache if exists.
				$cache = new Cargus_Cache();

				$temp = $cache->get_cached_file( 'str' . $locality_id, true );
				if ( false !== $temp ) {
					// return the old data.
					$json = json_decode( $temp, true );
				}
			}

			return $json;
		}

		/**
		 * Call cargus localities api endpoint.
		 *
		 * @param int $county_id The conty id.
		 */
		public function get_localities( $county_id ) {
			// get streets.
			if ( is_null( $county_id ) ) {
				return array();
			}

			$json = array();
			// get api json.
			// Localities?countryId=1&countyId=97243.
			$json = $this->call_method( 'Localities?countryId=1&countyId=' . $county_id, 'GET', array(), get_option( 'cargus_login_token' ) );

			if ( ! $json ) {
				// get old cache if exists.
				$cache = new Cargus_Cache();

				$temp = $cache->get_cached_file( 'localities' . $county_id, true );
				if ( false !== $temp ) {
					// return the old data.
					$json = json_decode( $temp, true );
				}
			}

			return $json;
		}

		/**
		 * Call cargus counties api endpoint.
		 */
		public function get_counties() {
			$json = array();

			// get api json.
			// Counties?countryId=1.
			$json = $this->call_method( 'Counties?countryId=1', 'GET', array(), get_option( 'cargus_login_token' ) );
			if ( ! $json ) {
				// get old cache if exists.
				$cache = new Cargus_Cache();
				$temp  = $cache->getCacheFile( 'counties', true );
				if ( false !== $temp ) {
					// return the old data.
					$json = json_decode( $temp, true );
				}
			}

			return $json;
		}

		/**
		 * Call cargus counties api endpoint.
		 */
		public function get_countries() {
			$json = array();

			// get api json.
			// Counties?countryId=1.
			$json = $this->call_method( 'intl-countries', 'GET', array(), get_option( 'cargus_login_token' ) );
			if ( ! $json ) {
				// get old cache if exists.
				$cache = new Cargus_Cache();
				$temp  = $cache->getCacheFile( 'countries', true );
				if ( false !== $temp ) {
					// return the old data.
					$json = json_decode( $temp, true );
				}
			}

			return $json;
		}

		/**
		 * Call cargus pickup locations api endpoint.
		 */
		public function get_shipment_status( $awb ) {
			$json = $this->call_method( 'AwbStatus/GetAwbSyncStatusByBarCode?barCode=' . $awb, 'GET', array(), get_option( 'cargus_login_token' ) );

			return $json;
		}

		/**
		 * Call cargus localities api endpoint for Bulgaria.
		 */
		public function get_localities_bulgaria() {

			$json = array();
			// get api json.
			// Localities?countryId=1&countyId=97243.
			$debugger = CargusDebug::get_instance();

			$json = $this->call_method_intl( 'cities?countryId=2', 'GET', array(), $this->token, true );
			$debugger->debug( 'Localities Bulgaria JSON: ' . wp_json_encode( $json ) );

			if ( ! $json ) {
				// get old cache if exists.
				$cache = new Cargus_Cache();

				$temp = $cache->get_cached_file( 'localities_bulgaria', true );
				if ( false !== $temp ) {
					// return the old data.
					$json = json_decode( $temp, true );
				}
			}

			return $json;
		}
	}
}
