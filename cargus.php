<?php
/**
 * The plugin bootstrap file
 *
 * @link              https://cargus.ro/
 * @since             1.0.0
 * @package           Cargus
 *
 * @wordpress-plugin
 * Plugin Name:       Cargus
 * Plugin URI:        http://woocommerce.demo.cargus.ro/
 * Description:       Metoda de livrare Cargus pentru WooCommerce.
 * Version:           1.5.0
 * Author:            Cargus
 * Author URI:        https://cargus.ro/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       cargus
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 */
define( 'CARGUS_VERSION', '1.5.0' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-cargus.php';
require_once plugin_dir_path( __FILE__ ) . 'admin/class-cargus-debug.php';
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_cargus() {

	$plugin = new Cargus();
	$plugin->run();

	// Hook to admin menu to add custom tools page
	add_action( 'admin_menu', 'cargus_add_tools_page' );

	// Handle file delete request
	add_action( 'admin_init', 'cargus_handle_delete_log' );

	add_action( 'wp_ajax_cargus_delete_log', 'cargus_delete_log' );

	function cargus_delete_log() {
		// Check if the user has the necessary capability
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( 'Unauthorized', 'Unauthorized', array( 'response' => 403 ) );
		}

		// Verify nonce for security
		check_admin_referer( 'cargus_delete_log', 'cargus_delete_log_nonce' );

		// Path to the cargus_debug.log file
		$file_path = WP_CONTENT_DIR . '/cargus_debug.log';

		// Check if the file exists and delete it
		if ( file_exists( $file_path ) ) {
			if ( unlink( $file_path ) ) {
				wp_redirect( admin_url( 'admin.php?page=cargus_tools&message=log_deleted' ) );
			} else {
				wp_die( 'Failed to delete the log file', 'Error', array( 'response' => 500 ) );
			}
		} else {
			wp_die( 'Log file not found', 'Error', array( 'response' => 404 ) );
		}
		exit;
	}

	add_action( 'wp_ajax_cargus_delete_specific_file', 'cargus_delete_specific_file' );

	function cargus_delete_specific_file() {

		$debugger = CargusDebug::get_instance();
		// Check if the user has the necessary capability
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_send_json_error( array( 'message' => 'Unauthorized' ) );
			wp_die();
		}

		// Verify nonce for security
		check_admin_referer( 'cargus_delete_file', 'cargus_delete_file_nonce' );

		// Get the file name from the request
		$file_name = isset( $_POST['file_name'] ) ? sanitize_text_field( $_POST['file_name'] ) : '';

		$debugger->log( 'file_name', $_POST['file_name'] );
		$debugger->log( 'file_name', $file_name );

		// Path to the file in the /admin/locations/ directory
		$file_path = plugin_dir_path( __DIR__ ) . 'admin/locations/' . $file_name;

		// Check if the file exists and delete it
		if ( file_exists( $file_path ) ) {
			if ( unlink( $file_path ) ) {
				wp_send_json_success( array( 'message' => 'File successfully deleted.' ) );
			} else {
				wp_send_json_error( array( 'message' => 'Failed to delete the file.' ) );
			}
		} else {
			wp_send_json_error( array( 'message' => 'File not found.' ) );
		}
		wp_die();
	}

	function cargus_tools_page() {
		// Directory containing the files to display and delete
		$directory = plugin_dir_path( __DIR__ ) . '/cargus/admin/locations/';

		// List of files to exclude
		$exclude_files = array( 'cttd.json', 'countries.json', 'counties.json', 'pudo_locations.json' );

		// Check if the directory exists
		if ( is_dir( $directory ) ) {
			// Get all files in the directory
			$files = array_diff( scandir( $directory ), array( '..', '.' ) );
		} else {
			$files = array(); // Set to empty array if directory doesn't exist
		}

		?>
		<div class="wrap">
			<h1>Cargus Debug Tool</h1>
			<p>Delete specific files from /admin/locations/ directory (excluding cttd.json, countries.json, counties.json):</p>
	
			<?php
			// Check if there are any files in the directory
			if ( $files ) {
				foreach ( $files as $file ) {
					// Skip excluded files
					if ( in_array( $file, $exclude_files ) ) {
						continue;
					}
					?>
					<!-- Form for Deleting Each File -->
					<form method="post" action="<?php echo esc_url( admin_url( 'admin-ajax.php?action=cargus_delete_specific_file' ) ); ?>">
						<?php wp_nonce_field( 'cargus_delete_file', 'cargus_delete_file_nonce' ); ?>
						<input type="hidden" name="file_name" value="<?php echo esc_attr( $file ); ?>">
						<input type="submit" class="button button-secondary" value="<?php echo esc_html( $file ); ?>">
					</form>
					<?php
				}
			} else {
				echo '<p>No files found in the directory.</p>';
			}
			?>
	
			<h2>Manage Debug Log</h2>
	
			<!-- Download Button for cargus_debug.log -->
			<a href="<?php echo esc_url( admin_url( 'admin-ajax.php?action=cargus_download_log' ) ); ?>" class="button">Download Debug Log (cargus_debug.log)</a>
	
			<!-- Delete Button for cargus_debug.log -->
			<form method="post" action="<?php echo esc_url( admin_url( 'admin-ajax.php?action=cargus_delete_log' ) ); ?>">
				<?php wp_nonce_field( 'cargus_delete_log', 'cargus_delete_log_nonce' ); ?>
				<input type="submit" class="button button-secondary" value="Delete Debug Log (cargus_debug.log)">
			</form>
	
			<!-- Display success message if a file is deleted -->
			<?php if ( isset( $_GET['message'] ) ) : ?>
				<?php if ( $_GET['message'] === 'file_deleted' ) : ?>
					<div class="updated notice is-dismissible">
						<p>File successfully deleted.</p>
					</div>
				<?php elseif ( $_GET['message'] === 'log_deleted' ) : ?>
					<div class="updated notice is-dismissible">
						<p>Debug log successfully deleted.</p>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
		<?php
	}

	function cargus_add_tools_page() {
		add_management_page(
			'Cargus Debug Tool', // Page title
			'Cargus Debug Tool', // Menu title
			'manage_options',    // Capability
			'cargus-debug-tool', // Menu slug
			'cargus_tools_page'  // Callback function
		);
	}

	function cargus_handle_delete_log() {
		// Check if the delete request is sent
		if ( isset( $_POST['action'] ) && $_POST['action'] === 'cargus_delete_log' ) {
			// Check the nonce for security
			if ( ! isset( $_POST['cargus_delete_log_nonce'] ) || ! wp_verify_nonce( $_POST['cargus_delete_log_nonce'], 'cargus_delete_log' ) ) {
				wp_die( 'Nonce verification failed', 'Error', array( 'response' => 403 ) );
			}

			// Check if user has the necessary capability
			if ( ! current_user_can( 'manage_options' ) ) {
				wp_die( 'Unauthorized', 'Unauthorized', array( 'response' => 403 ) );
			}

			// Path to the log file
			$log_file = WP_CONTENT_DIR . '/cargus_debug.log';

			if ( file_exists( $log_file ) ) {
				// Attempt to delete the file
				if ( unlink( $log_file ) ) {
					// Redirect with success message
					wp_redirect( add_query_arg( 'cargus_log_deleted', 'true', wp_get_referer() ) );
					exit;
				} else {
					wp_die( 'Failed to delete the log file', 'Error', array( 'response' => 500 ) );
				}
			} else {
				wp_die( 'Log file not found', 'Error', array( 'response' => 404 ) );
			}
		}
	}

	// Display a success message if the log file was deleted
	add_action( 'admin_notices', 'cargus_admin_notices' );

	function cargus_admin_notices() {
		if ( isset( $_GET['cargus_log_deleted'] ) && $_GET['cargus_log_deleted'] === 'true' ) {
			echo '<div class="notice notice-success is-dismissible"><p>Debug log file has been deleted.</p></div>';
		}
	}
}

run_cargus();
